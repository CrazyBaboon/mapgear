# MapGear - Map processing tools for FlightGear

For more screenshots, news and requests, check https://forum.flightgear.org/viewtopic.php?f=6&t=31741

![Alt Text](http://i.imgur.com/8dgqt3L.gif)

![Alt Text](http://i.imgur.com/GemTzb8.gif)

![Alt Text](http://i.imgur.com/SFoztiC.gif)

Short youtube tutorial on how to use MapGear.py: https://www.youtube.com/watch?v=0BaVbCtOr3E

# How to use MapGear:

1.Extract the contents of MapGear-master.zip into your /home folder.

2.Run in the terminal: fgfs --launcher  && python MapGear.py

In order to save .GIF files, uncomment the last line of the script. Note: it will take a few minutes to save the .GIF file.





Sit back and enjoy. Everything should now be automatic!

Suggestions and improvements are welcome!

# Requires the following Python libraries:

-basemap;

-matplotlib;

-geopy;

-pandas;

-numpy;

-pyshp;
