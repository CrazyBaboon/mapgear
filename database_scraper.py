# FlightGear database scraper (C) Nuno Ferreira 2018
# Licensed under the GNU GPL version 2+ 

from urllib.request import urlopen
from bs4 import BeautifulSoup
import re
import numpy as np
import pandas as pd
import time

start_time = time.time()

number_of_static_models = 7325 # as of November 2018

data_numpy = np.zeros( (number_of_static_models,3) ) # create the numpy array

for n in range( number_of_static_models ):
    time.sleep(.200) # be nice on the server!
    url = "https://scenery.flightgear.org/app.php?c=Objects&a=search&model="+str(n)
    html = urlopen(url)
    soup = BeautifulSoup(html, 'lxml')

    try:
        coord_tmp = soup.find_all('td')[5]
        coord = re.findall(r"[-+]?\d*\.\d+|\d+", str(coord_tmp) ) # extract values out of a string

        data_numpy[n][0] = n + 1
        data_numpy[n][1] = coord[0]
        data_numpy[n][2] = coord[1]

        print(n + 1)
        print('Model %d parsed.' % (n + 1))

    except IndexError:
        data_numpy[n][0] = n + 1
        data_numpy[n][1] = np.NaN
        data_numpy[n][2] = np.NaN

        print(n + 1)
        print('Oops! This model %d has 0 occurences!' % (n + 1))

    
data_df = pd.DataFrame({'# model':data_numpy[:,0],'Longitude':data_numpy[:,1],'Latitude':data_numpy[:,2]})
data_df.to_csv('flightGear_positions.csv')

print("--- Elapsed time = %s seconds ---" % (time.time() - start_time))